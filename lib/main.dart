import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int value = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('value.appBar'),

      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 200,
            ),
            Center(
              child: Consumer<MyProvider>(

                builder: (context,value,_) {
                  print('asd');
                  return DropdownButton<int>(
                    value: value.index,
                    onChanged: (value) => context.read<MyProvider>().changeAppBar(value!),
                    items: List.generate(
                      20,
                      (index) => DropdownMenuItem(
                        child: Text('${index}'),
                        value: index,
                      ),
                    ),
                  );
                }
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyProvider with ChangeNotifier {
  int _index = 0;

 int get index => _index;

  void changeAppBar(int int) {
    _index = int;

    notifyListeners();
  }
}
